import 'package:flutter/material.dart';
import 'package:mtn_module_3/profile.dart';

import 'abstract_widgets/primary_button.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Dashboard",style: TextStyle(color: Colors.white,fontSize: 25.0,fontWeight: FontWeight.bold))),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            PrimaryButton(buttonName: "Feature 1", onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const DashboardView()),
              );
            }),
            PrimaryButton(buttonName: "Feature 2", onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const DashboardView()),
              );
            }),
            PrimaryButton(buttonName: "Edit Profile", onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ProfileView()),
              );
            }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.android_outlined),
        backgroundColor: Colors.cyan,
        onPressed: () {

        },
      ),
    );
  }
}
