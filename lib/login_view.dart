
import 'package:flutter/material.dart';
import 'package:mtn_module_3/signup_view.dart';

import 'abstract_widgets/primary_button.dart';
import 'dashboard_view.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Login",style: TextStyle(color: Colors.white,fontSize: 25.0,fontWeight: FontWeight.bold)),),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
           Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: const [
               SizedBox(
                 height: 50,
               ),
               Text("Name/Username"),
               TextField(),
               SizedBox(
                 height: 20,
               ),
               Text("Password"),
               TextField(),
               SizedBox(
                 height: 50,
               ),
             ],
           ),
           Column(
             crossAxisAlignment: CrossAxisAlignment.center,
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
             children: [
               PrimaryButton(buttonName: "Login", onTap: (){
                 Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => const DashboardView()),
                 );
               }),
               const SizedBox(
                 height: 20.0,
               ),
               PrimaryButton(buttonName: "Sign Up", onTap: (){
                 Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => const SignUpView()),
                 );
               })
             ],
           ),
          ],
        ),
      ),
    );
  }
}
