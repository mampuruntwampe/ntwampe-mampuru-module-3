
import 'package:flutter/material.dart';
import 'package:mtn_module_3/dashboard_view.dart';

import 'abstract_widgets/primary_button.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Edit Profile"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset("assets/user.png",width: 100,height: 100,),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Full Name"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Username"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Email"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Password"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  PrimaryButton(buttonName: "Update", onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const DashboardView()),
                    );
                  }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
