
import 'package:flutter/material.dart';
import 'package:mtn_module_3/dashboard_view.dart';
import 'package:mtn_module_3/login_view.dart';

import 'abstract_widgets/primary_button.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Sign Up"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              const Text("Full Name"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Username"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Email"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Password"),
              const TextField(),
              const SizedBox(
                height: 20,
              ),
              const Text("Confirm Password"),
              const TextField(),
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  PrimaryButton(buttonName: "Sign Up",width: 150, onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const DashboardView()),
                    );
                  }),
                  PrimaryButton(buttonName: "Login",width: 150, onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const LoginView()),
                    );
                  })
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
